package com.demo.clase7.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonaDto {

    private Long id;
    private String nombre, email, direccion, telefono;

    private Long empresa_id;

}
