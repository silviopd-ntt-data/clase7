package com.demo.clase7.Dto;

import com.demo.clase7.Entity.Persona;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor

public class UsuarioDto {

    private String username, password;

    private Long empleadoId;
}
