package com.demo.clase7.Controller;

import com.demo.clase7.Entity.Rol;
import com.demo.clase7.Service.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rol")
public class RolController {
    @Autowired
    private IRolService rolService;

    @RequestMapping("/listar")
    public List<Rol> listar() {
        return rolService.listar();
    }

    @PostMapping("/registrar")
    public Rol crear(@RequestBody Rol rol) {
        return rolService.registrar(rol);
    }

    @PutMapping("/actualizar/{id}")
    public Rol actualizar(@PathVariable("id") Long id, @RequestBody Rol rol) {
        return rolService.modificar(id, rol);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable("id") Long id) {
        return rolService.eliminar(id);
    }

}
