package com.demo.clase7.Controller;

import com.demo.clase7.Entity.Empresa;
import com.demo.clase7.Entity.Persona;
import com.demo.clase7.Service.IEmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    private IEmpresaService empresaService;

    @RequestMapping("/listar")
    public List<Empresa> listar() {
        return empresaService.listar();
    }

    @PostMapping("/registrar")
    public Empresa crear(@RequestBody Empresa empresa) {
        return empresaService.registrar(empresa);
    }

    @PutMapping("/actualizar/{id}")
    public Empresa actualizar(@PathVariable("id") Long id, @RequestBody Empresa empresa) {
        return empresaService.modificar(id, empresa);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable("id") Long id) {
        return empresaService.eliminar(id);
    }
}
