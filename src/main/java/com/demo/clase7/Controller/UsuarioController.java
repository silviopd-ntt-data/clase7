package com.demo.clase7.Controller;

import com.demo.clase7.Dto.PersonaDto;
import com.demo.clase7.Dto.UsuarioDto;
import com.demo.clase7.Entity.Persona;
import com.demo.clase7.Entity.Usuario;
import com.demo.clase7.Service.UsuarioServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioServiceImpl usuarioService;

    @GetMapping("/listar")
    public List<Usuario> listar() {
        return usuarioService.listar();
    }

    @PostMapping("/registrar")
    public Usuario crear(@RequestBody Usuario persona) {
        return usuarioService.registrar(persona);
    }

    @PutMapping("/actualizar/{id}")
    public Usuario actualizar(@PathVariable("id") Long id, @RequestBody Usuario persona) {
        return usuarioService.modificar(id, persona);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable("id") Long id) {
        return usuarioService.eliminar(id);
    }


    @PostMapping("/registrardto")
    public Usuario crear2(@RequestBody UsuarioDto usuarioDto) {
        return usuarioService.registrarDto(usuarioDto);
    }

    @PostMapping("/registrarconempleado")
    public Usuario crear3(@RequestBody Usuario usuario) {
        return usuarioService.registrarUsuarioEmpleado(usuario);
    }
}


