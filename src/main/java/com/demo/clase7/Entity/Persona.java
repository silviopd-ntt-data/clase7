package com.demo.clase7.Entity;

import com.demo.clase7.Entity.Empresa;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "persona")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre, email, direccion, telefono;

    @ManyToOne()
    @JoinColumn(name = "empresa_id")
    private Empresa empresa;
}
