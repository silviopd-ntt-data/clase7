package com.demo.clase7.Service;

import com.demo.clase7.Entity.Empresa;

import java.util.List;

public interface IEmpresaService {

    //    CRUD
    public List<Empresa> listar();

    public Empresa registrar(Empresa empresa);

    public Empresa modificar(Long id, Empresa empresa);

    public String eliminar(Long id);
}
