package com.demo.clase7.Service;

import com.demo.clase7.Entity.Empresa;
import com.demo.clase7.Repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmpresaServiceImpl implements IEmpresaService {

    @Autowired
    public EmpresaRepository empresaRepository;

    @Override
    public List<Empresa> listar() {
        return empresaRepository.findAll();
    }

    @Override
    public Empresa registrar(Empresa empresa) {
        return empresaRepository.save(empresa);
    }

    @Override
    public Empresa modificar(Long id, Empresa empresa) {
        Optional<Empresa> e = empresaRepository.findById(id);
        if (e.isPresent()) {
            Empresa e2 = new Empresa();
            e2.setId(id);
            e2.setRazonSocial(empresa.getRazonSocial() != null ? empresa.getRazonSocial() : e.get().getRazonSocial());
            e2.setRuc(empresa.getRuc() != null ? empresa.getRuc() : e.get().getRuc());
            e2.setRepresentante(empresa.getRepresentante() != null ? empresa.getRepresentante() : e.get().getRepresentante());
            e2.setFechaCreacion(empresa.getFechaCreacion() != null ? empresa.getFechaCreacion() : e.get().getFechaCreacion());
            return empresaRepository.save(e2);
        }
        return null;
    }

    @Override
    public String eliminar(Long id) {
        Optional<Empresa> e = empresaRepository.findById(id);
        if (e.isPresent()) {
            empresaRepository.deleteById(id);
            return "Eliminado Correctamente";
        }
        return "No se encontro el registro";
    }
}
