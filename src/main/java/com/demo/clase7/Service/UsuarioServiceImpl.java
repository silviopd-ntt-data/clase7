package com.demo.clase7.Service;

import com.demo.clase7.Dto.UsuarioDto;
import com.demo.clase7.Entity.Persona;
import com.demo.clase7.Entity.Usuario;
import com.demo.clase7.Entity.Usuario;
import com.demo.clase7.Repository.PersonaRepository;
import com.demo.clase7.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private PersonaRepository personaRepository;

    @Override
    public List<Usuario> listar() {
        return usuarioRepository.findAll();
    }

    @Override
    public Usuario registrar(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    //modificar la edición de usuario
    @Override
    public Usuario modificar(Long id, Usuario usuario) {
        Optional<Usuario> u = usuarioRepository.findById(id);
        if (u.isPresent()) {
            Usuario u2 = new Usuario();
            u2.setId(id);
            u2.setUsername(usuario.getUsername() != null ? usuario.getUsername() : u.get().getUsername());
            u2.setPassword(usuario.getPassword() != null ? usuario.getPassword() : u.get().getPassword());
            return usuarioRepository.save(u2);
        }
        return null;
    }

    @Override
    public String eliminar(Long id) {
        Optional<Usuario> u = usuarioRepository.findById(id);
        if (u.isPresent()) {
            usuarioRepository.deleteById(id);
            return "Eliminado Correctamente";
        }
        return "No se encontro el registro";
    }

    //Probar el servicio
    @Override
    public Usuario registrarDto(UsuarioDto usuarioDto) {
        Optional<Persona> p = personaRepository.findById(usuarioDto.getEmpleadoId());
        if (p.isPresent()) {
            Usuario u = new Usuario();
            u.setUsername(usuarioDto.getUsername());
            u.setPassword(usuarioDto.getPassword());
            u.setPersona(p.get());
            return usuarioRepository.save(u);
        }
        return null;
    }

    @Override
    public Usuario registrarUsuarioEmpleado(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }
}
