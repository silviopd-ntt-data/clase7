package com.demo.clase7.Service;

import com.demo.clase7.Dto.UsuarioDto;
import com.demo.clase7.Entity.Usuario;

import java.util.List;

public interface IUsuarioService {

    //    CRUD
    public List<Usuario> listar();

    public Usuario registrar(Usuario usuario);

    public Usuario modificar(Long id, Usuario usuario);

    public String eliminar(Long id);

    public Usuario registrarDto(UsuarioDto usuarioDto);

    Usuario registrarUsuarioEmpleado(Usuario usuario);
}
