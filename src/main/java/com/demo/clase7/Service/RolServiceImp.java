package com.demo.clase7.Service;

import com.demo.clase7.Entity.Rol;
import com.demo.clase7.Entity.Rol;
import com.demo.clase7.Repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RolServiceImp implements IRolService {

    @Autowired
    private RolRepository rolRepository;

    @Override
    public List<Rol> listar() {
        return rolRepository.findAll();
    }

    @Override
    public Rol registrar(Rol rol) {
        return rolRepository.save(rol);
    }

    @Override
    public Rol modificar(Long id, Rol rol) {
        Optional<Rol> r = rolRepository.findById(id);
        if (r.isPresent()) {
            Rol r2 = new Rol();
            r2.setId(id);
            r2.setNombre(rol.getNombre() != null ? rol.getNombre() : r.get().getNombre());
            return rolRepository.save(r2);
        }
        return null;
    }

    @Override
    public String eliminar(Long id) {
        Optional<Rol> r = rolRepository.findById(id);
        if (r.isPresent()) {
            rolRepository.deleteById(id);
            return "Eliminado Correctamente";
        }
        return "No se encontro el registro";
    }
}
